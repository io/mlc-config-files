DELIMITER $$

USE `voxbox-backend`$$

DROP FUNCTION IF EXISTS `fnNumber2DestinationID`$$

CREATE FUNCTION `fnNumber2DestinationID`(param_b_number VARCHAR(50) ) RETURNS int
BEGIN
	DECLARE found_tech_prefix VARCHAR(50);
	DECLARE found_destination_id int;
	DECLARE number_substring VARCHAR(2);
	
	-- checking if any tech prefix is provided so can be removed
	SELECT inbound_prefix 
	INTO found_tech_prefix 
	FROM `VAR_HOSTNAME_development`.`interconnections`
	WHERE POSITION(inbound_prefix IN param_b_number) = 1
	ORDER BY LENGTH(inbound_prefix) DESC
	LIMIT 1;
	-- if found any tech prefix removing it
	IF found_tech_prefix IS NOT NULL THEN
		SET param_b_number = REPLACE(param_b_number, found_tech_prefix, '');
	END IF;

	-- substring
	SELECT SUBSTRING(param_b_number,1,2) INTO number_substring;	
	
	-- finally check for destination name
	SELECT `zone_id`
	INTO found_destination_id
	FROM `VAR_HOSTNAME_development`.`codes`
	USE INDEX (prefix_idx, PRIMARY)
	WHERE POSITION(prefix IN param_b_number) = 1
	AND prefix LIKE CONCAT(number_substring, '%')
	ORDER BY LENGTH(prefix) DESC
	LIMIT 1;
	
	RETURN found_destination_id;
    END$$

DELIMITER ;
