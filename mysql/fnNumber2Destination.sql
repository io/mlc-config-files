DELIMITER $$

USE `voxbox-backend`$$

DROP FUNCTION IF EXISTS `fnNumber2Destination`$$

CREATE FUNCTION `fnNumber2Destination`(param_b_number VARCHAR(50) ) RETURNS VARCHAR(150) CHARSET latin1
BEGIN
	DECLARE found_tech_prefix VARCHAR(50);
	DECLARE found_destination_name VARCHAR(150);
	DECLARE number_substring VARCHAR(2);
	
	-- checking if any tech prefix is provided so can be removed
	SELECT inbound_prefix 
	INTO found_tech_prefix 
	FROM `VAR_HOSTNAME_development`.`interconnections`
	WHERE POSITION(inbound_prefix IN param_b_number) = 1
	ORDER BY LENGTH(inbound_prefix) DESC
	LIMIT 1;
	-- if found any tech prefix removing it
	IF found_tech_prefix IS NOT NULL THEN
		SET param_b_number = REPLACE(param_b_number, found_tech_prefix, '');
	END IF;
	
	-- substring
	SELECT SUBSTRING(param_b_number,1,2) INTO number_substring;	
	
	-- finally check for destination name
	SELECT `name`
	INTO found_destination_name 
	FROM `VAR_HOSTNAME_development`.`codes`
	USE INDEX (prefix_idx, PRIMARY)
	WHERE POSITION(prefix IN param_b_number) = 1
	AND prefix LIKE CONCAT(number_substring, '%')
	ORDER BY LENGTH(prefix) DESC
	LIMIT 1;
	
	RETURN found_destination_name;
    END$$

DELIMITER ;
