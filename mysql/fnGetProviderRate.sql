DELIMITER $$

USE `voxbox-backend`$$

DROP FUNCTION IF EXISTS `fnGetProviderRate`$$

CREATE FUNCTION `voxbox-backend`.`fnGetProviderRate`(param_b_number VARCHAR(50), param_supplier_rate_id INT ) RETURNS DECIMAL(6,4)
    BEGIN

	DECLARE found_rate DECIMAL(6,4);
	
	-- select provider rate by RateID and closest Prefix Match
	SELECT `rate_per_minute`
	INTO found_rate 
	FROM `VAR_HOSTNAME_development`.`supplier_rate_details`
	WHERE POSITION(prefix IN param_b_number) = 1
	AND supplier_rate_id = param_supplier_rate_id
	ORDER BY LENGTH(prefix) DESC
	LIMIT 1;
	
	RETURN found_rate;

    END$$

DELIMITER ;
