insert into zones (name, atla_id, created_at) values ('Italy Mobile TIM', 111, now() );
insert into zones (name, atla_id, created_at) values ('Italy Mobile Vodafone', 111, now() );
insert into zones (name, atla_id, created_at) values ('Italy Mobile Wind', 111, now() );
insert into zones (name, atla_id, created_at) values ('Italy Mobile H3G', 111, now() );
insert into zones (name, atla_id, created_at) values ('Italy Mobile Others', 111, now() );


update codes set name='Italy Mobile H3G', zone_id=754 where (name like 'Ita%' and name like '%H3G%');
update codes set name='Italy Mobile TIM', zone_id=751 where (name like 'Ita%' and name like '%TIM%');
update codes set name='Italy Mobile Vodafone', zone_id=752 where (name like 'Ita%' and name like '%Vodafone%');
update codes set name='Italy Mobile Wind', zone_id=753 where (name like 'Ita%' and name like '%WIND%');
update codes set name='Italy Mobile Others', zone_id=755 where (name like 'Ita%' and name like '%Mobile%') and zone_id not in (751, 752, 753, 754,755);

delete from zones where id=335;
