DELIMITER $$

USE `voxbox-backend`$$

DROP FUNCTION IF EXISTS `fnGetCustomerRate`$$

CREATE FUNCTION `voxbox-backend`.`fnGetCustomerRate`(param_b_number varchar(50), param_customer_rate_id int ) RETURNS decimal(6,4)
BEGIN
    DECLARE found_destination_id int;
	DECLARE found_rate DECIMAL(6,4);
	
	SELECT `zone_id`
	INTO found_destination_id 
	FROM `VAR_HOSTNAME_development`.`codes`
	WHERE POSITION(prefix IN param_b_number) = 1
	ORDER BY LENGTH(prefix) DESC
	LIMIT 1;	
	
	-- select customer rate by RateID and closest Prefix Match
	
	SELECT `rate_per_minute`
	INTO found_rate 
	FROM `VAR_HOSTNAME_development`.`customer_rate_details`
	WHERE zone_id  = found_destination_id
	AND customer_rate_id = param_customer_rate_id
	AND now() >= customer_rate_details.effective_date
	LIMIT 1;

	
	RETURN found_rate;

    END$$

DELIMITER ;
