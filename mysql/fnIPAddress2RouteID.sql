DELIMITER $$

USE `voxbox-backend`$$

DROP FUNCTION IF EXISTS `fnIPAddress2RouteID`$$

CREATE FUNCTION `fnIPAddress2RouteID`(param_ip_address VARCHAR(50) ) RETURNS int
BEGIN
	DECLARE found_route_id int;
	
	-- get current route ID
	SELECT ser.route_id
	INTO found_route_id 
	FROM `VAR_HOSTNAME_development`.ip_addresses ip, `VAR_HOSTNAME_development`.interconnections ser, `VAR_HOSTNAME_development`.carriers car
	WHERE ip.`interconnection_id` = ser.id
	AND ser.interconnection_type <> 'outbound'
	AND car.id=ser.carrier_id
	AND ip_address = param_ip_address
	LIMIT 1;
	
	RETURN found_route_id;
    END$$

DELIMITER ;
