DELIMITER $$

USE `voxbox-backend`$$

DROP FUNCTION IF EXISTS `fnNumber2TechPrefix`$$

CREATE DEFINER=`root`@`localhost` FUNCTION `fnNumber2TechPrefix`(param_b_number VARCHAR(50) ) RETURNS VARCHAR(150) CHARSET latin1
BEGIN
	DECLARE found_tech_prefix VARCHAR(50);
	
	-- checking if any tech prefix is provided so can be removed
	SELECT inbound_prefix 
	INTO found_tech_prefix 
	FROM `VAR_HOSTNAME_development`.`interconnections`
	WHERE POSITION(inbound_prefix IN param_b_number) = 1
	ORDER BY LENGTH(inbound_prefix) DESC
	LIMIT 1;
	
	RETURN found_tech_prefix;
    END$$

DELIMITER ;
