DELIMITER $$

USE `voxbox-backend`$$

DROP VIEW IF EXISTS `view_active_calls`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_active_calls` AS (
SELECT
  `car`.`name`              AS `carrier_name`,
  `car`.`id`                AS `carrier_id`,
  `ser`.`name`              AS `interconnection_name`,
  `ser`.`id`                AS `interconnection_id`,
  `chan`.`created`          AS `start_date`,
  SEC_TO_TIME(TIMESTAMPDIFF(SECOND,`chan`.`created`,NOW())) AS `elapsed_time`,
  `chan`.`cid_num`          AS `a_number`,
  `chan`.`dest`             AS `dialled_b_number`,
  (CASE WHEN (`fnNumber2TechPrefix`(`chan`.`dest`) IS NOT NULL) THEN RIGHT(`chan`.`dest`,(LENGTH(`chan`.`dest`) - LENGTH(`fnNumber2TechPrefix`(`chan`.`dest`)))) ELSE `chan`.`dest` END) AS `b_number`,
  `fnNumber2TechPrefix`(
`chan`.`dest`)  AS `tech_prefix`,
  `fnNumber2Destination`(
`chan`.`dest`)  AS `destination`,
  LEFT(REPLACE(`chan`.`application_data`,'sofia/gateway/',''),(LOCATE('/',REPLACE(`chan`.`application_data`,'sofia/gateway/','')) - 1)) AS `outbound_carrier`,
  `chan`.`dest`             AS `dest`,
  `chan`.`direction`        AS `direction`,
  `chan`.`application_data` AS `application_data`,
  `chan`.`callstate`        AS `call_state`,
  `chan`.`read_codec`       AS `in_codec`,
  `chan`.`write_codec`      AS `out_codec`
FROM (((`voxbox-backend`.`channels` `chan`
     JOIN `VAR_HOSTNAME_development`.`ip_addresses` `ip`)
    JOIN `VAR_HOSTNAME_development`.`interconnections` `ser`)
   JOIN `VAR_HOSTNAME_development`.`carriers` `car`)
WHERE ((`ip`.`interconnection_id` = `ser`.`id`)
       AND (`ser`.`interconnection_type` <> 'outbound')
       AND (`car`.`id` = `ser`.`carrier_id`)
       AND (CONVERT(`chan`.`ip_addr` USING utf8) = (`ip`.`ip_address` COLLATE utf8_unicode_ci))))$$

DELIMITER ;
